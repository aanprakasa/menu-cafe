include 'emu8086.inc'

cls macro 
    mov ax,0600h
    xor cx,cx
    mov dx,184fh
    mov bh,7
    int 10h
    endm

output macro string
    mov ah,09
    lea dx,string
    int 21h
    endm

input macro
    mov ah, 01h
    int 21h
    endm

   
.model small
.code
org 100h

Data:
    JMP Mulai
    
    menu    db 13,10,'+===================================================+',13,10
            db '|**********>>>>>>>>>>>MENU CAFE<<<<<<<<<<<**********|',13,10
            db '+===================================================+',13,10
            db '|1.Paket Irit (Nasi Ayam Sayap + Esteh)             |',13,10 
            db '|  Rp. 10.000,-                                     |',13,10
            db '|2.Paket Hemat (Nasi Ayam Paha Bawah + Esteh)       |',13,10 
            db '|  Rp. 12.500,-                                     |',13,10
            db '|3.Paket Kenyang 1 (Nasi Ayam Dada + Esteh)         |',13,10 
            db '|  Rp. 14.000,-                                     |',13,10
            db '|4.Paket Kenyang 2 (Nasi Ayam Paha Atas + Esteh)    |',13,10 
            db '|  Rp. 14.000,-                                     |',13,10
            db '|5.Paket Prekprek (Nasi Ayam Geprek + Esteh)        |',13,10 
            db '|  Rp. 17.000,-                                     |',13,10
            db '|6.Paket Prekprek 2 (Nasi Ayam Geprek Keju + Esteh) |',13,10
            db '|  Rp. 18.500,-                                     |',13,10
            db '+===================================================+',13,10,'$' 
            
    pil1    db 13,10,'  Membeli Paket Irit Rp. 10.000$'
    pil2    db 13,10,'  Membeli Paket Hemat Rp. 12.500$'
    pil3    db 13,10,'  Membeli Paket Kenyang1 Rp. 14.000$'
    pil4    db 13,10,'  Membeli Paket Kenyang2 Rp. 14.000$'
    pil5    db 13,10,'  Membeli Paket Prekprek1 Rp. 17.000$'
    pil6    db 13,10,'  Membeli Paket Prekprek2 Rp. 18.500$'
    
    pert1   db 13,10,' Menu yang anda pilih:$'
    
Mulai:
    cls
    output menu
    putc 13
    putc 10
    
Mulai2:
    output pert1

pilih:
    input
    putc 13
    putc 10
    cmp al, '1'
    je sa
    cmp al, '2'
    je du
    cmp al, '3'
    je ti
    cmp al, '4'
    je em
    cmp al, '5'
    je li
    cmp al, '6'
    je en
    jne Mulai2   

sa:
    mov ah,09h
    lea dx,pil1
    int 21h
    jmp selesai
    
du:
    mov ah,09h
    lea dx,pil2
    int 21h
    jmp selesai
    
ti:    
    mov ah,09h
    lea dx,pil3
    int 21h
    jmp selesai
    
em:
    mov ah,09h
    lea dx,pil4
    int 21h
    jmp selesai
        
li:
    mov ah,09h
    lea dx,pil5
    int 21h
    jmp selesai
        
en:
    mov ah,09h
    lea dx,pil6
    int 21h
    jmp selesai

selesai:    
    end data
    end program    
                        
